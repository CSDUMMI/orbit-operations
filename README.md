# Satellite

OrbitDB based library for building operations based OrbitDB Apps
and making all unnecessary complexity optional.

This library automatically sets up IPFS and OrbitDB
to be deployable in the Browser & on NodeJS.

## Install
```bash
npm i --save orbit-satellite
```

## Usage
```js
const Satellite = require("orbit-satellite")

const inc = {
  name: "inc",
  apply(state, entry = { value: 1 }) {
    state.value += entry.value
    return state
  },
}

Satellite.addStore({
    type: "counter",
    operations: [inc],
    initialState: { value: 0 },
})

async function main() {
  const satellite = await Satellite.createInstance()

  const db = await satellite.create("someValues", "counter")

  await db.inc()

  console.log(db.state.value)
}
```

[See examples](https://gitlab.com/csdummi/orbit-satellite/examples/)

## Browsers
To change the IPFS and OrbitDB configuration,
change the `Satellite.ipfsConfig` and `Satellite.orbitdbConfig`.

These are configured already for use in the Browser.
To use it in the browser,
add the stores, export `Satellite`
and browserify the module.


```js
module.exports = Satellite
```