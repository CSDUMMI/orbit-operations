const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")
const SatelliteStore = require("./SatelliteStore")

class Satellite {
  /**
   * @param options.type {string} of the store
   * @param operations {Array<operation>} to modify the state of the store. 
   * @param initialState {any} of the store.
   */
  static addStore(options = {
    type: undefined,
    operations: [],
    initialState: {},
  }) {

    if(options.type === undefined) throw `options.type not defined`

    options.type = `${options.type}-sat`

    let operations = {}
    options.operations.forEach(op => {
      operations[op.name] = op
    })

    options.operations = operations
    
    const CustomStore = SatelliteStore(options.type, options.operations, options.initialState)
    OrbitDB.addDatabaseType(options.type, CustomStore)
  }

  /**
   * Create a store previously added via addStore.
   * @param name {string} of the db
   * @param type {string} of the db
   * @param writeAccess {Array<string|identity>} defaults to a public database, but can be restricted.
   * @param options {object} further options to be given to orbitdb.create.
   */
  async create(name, type, options = { open: true, meta: undefined }, writeAccess = undefined) {
    const db = await this.orbitdb.create(name, `${type}-sat`, {
      accessController: {
        write: writeAccess === undefined ? ["*"] : writeAccess
      },
      ...options,
    })

    return db
  }

  /**
   * @param address {string|OrbitDBAddress} of the db to open.
   *
   * You can fetch the address through db.address
   * and pass it to open.
   */
  async open(address) {
    const db = await this.orbitdb.open(address)
    return db
  }

  async stop() {
    await this.ipfs.stop()
  }
  
  get identity() {
    return this.orbitdb.identity
  }

  /**
   * Default ipfs Configuration.
   * You should modify it to customize it to your requirements.
   *
   * But it is attempted to make this configuration
   * ready for deployment as best as possible.
   */
  static ipfsConfig = {
    repo: "./jsipfs",
    relay: {
      enabled: true,
      hop: {
        enabled: true,
        active: true,
      }
    },
    preload: { enabled: false },
    config: {
      Addresses: {
        Swarm: [
          '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
          '/dns4/wrtc-star2.sjc.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
          '/dns4/webrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star/',
        ],
      },
      Swarm: {
        enableRelayHop: true,
      }
    },
  }

  /**
   * orbitdb configuration used by default by createInstance.
   * 
   * See ipfsConfig for details.
   */
  static orbitdbConfig = {
    directory: "./orbitdb",
  }

  /**
   * @param orbitdbConfig defaults to Satellite.orbitdbConfig
   * @param ipfsConfig defaults to Satellite.ipfsConfig
   * @returns {Satellite}
   */
  static async createInstance(orbitdbConfig = Satellite.orbitdbConfig,
                              ipfsConfig = Satellite.ipfsConfig
                             ) {
    const ipfs = await IPFS.create(ipfsConfig)
    const orbitdb = await OrbitDB.createInstance(ipfs, orbitdbConfig)
    return new Satellite(ipfs, orbitdb)
  }

  constructor(ipfs, orbitdb) {
    this.ipfs = ipfs
    this.orbitdb = orbitdb
  }
}

module.exports = Satellite
