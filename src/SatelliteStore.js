const OrbitDBStore = require("orbit-db-store")

function SatelliteIndex(operations, initialState, { meta, ipfs }) {
  class Index {
    constructor() {
      this._state = Object.assign({},
                                  typeof initialState == 'function'
                                  ? initialState(meta) : initialState)
    }

    updateIndex(oplog) {
      this._state = Object.assign({},
                                  typeof initialState == "function"
                                  ? initialState(meta) : initialState)
      oplog
        .values
        .slice()
        .reduce((handled, entry) => {
          if(!handled.includes(entry.hash)) {
            this._state = operations[entry.payload.op].apply(this._state, entry.payload.data, {
              identity: entry.identity,
              clock: entry.clock,
              hash: entry.hash,
              meta: meta,
              ipfs: ipfs,
            })

            handled.push(entry.hash)
            return handled
          }
        }, [])
    }
  }
  
  return Index
}

function SatelliteStore(type, operations, initialState) {

  class Store extends OrbitDBStore {
    static type = type

    constructor(ipfs, id, dbname, options) {
      let opts = Object.assign({}, {
        Index: SatelliteIndex(operations, initialState, {
          meta: options.meta,
          ipfs: ipfs,
        })
      })
      
      Object.assign(opts, options)
      console.log(opts.meta)
      super(ipfs, id, dbname, opts)
      this._type = Store.type
      
      Object
        .values(operations)
        .forEach(operation => {
          this[operation.name] = async (data, options) => {
            return {
              hash: await this._addOperation({
                op: operation.name,
                data: data,
              }, options)
            }
          }
        })
    }

    get meta() { return this._meta }

    get state() { return this._index._state }
  }

  return Store
}

module.exports = SatelliteStore
