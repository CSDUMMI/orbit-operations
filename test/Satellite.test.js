const Satellite = require("../src/Satellite")
const assert = require("assert")

const inc = {
  name: "inc",
  apply(state, value) {
    state.value += value
    return state
  }
}

describe("Satellite", () => {
  let satellite, db

  it("add custom counter", () => {
    Satellite.addStore({
      type: "counter",
      operations: [inc],
      initialState: { value: 0 },
    })
  })

  it("create counter db", async function() {
    this.timeout(0)

    Satellite.orbitdbConfig.directory = "./orbitdb" + Math.random()
    
    satellite = await Satellite.createInstance()

    db = await satellite.create("test", "counter")

    assert.equal(db.state.value, 0)
  })

  it("use inc", async function() {
    this.timeout(0)
    let inc = 5
    let i

    for(i = 1; i <= 20; i++) {
      await db.inc(inc)
      assert.equal(db.state.value, inc*i)
    }

    --i

    await db.inc(-1 * inc * i)
    assert.equal(db.state.value, 0)
  })

  after(async () => {
    return await satellite.stop()
  })
})

describe("Custom Store options", () => {
  let satellite, db

  it("add store with meta option", () => {
    Satellite.addStore({
      type: "counter-meta",
      operations: [inc],
      initialState(meta) {
        console.log(meta)
        return { value: meta["value"] ? meta["value"] : 0 }
      }
    })
  })

  it("create store with meta option", async () => {
    Satellite.orbitdbConfig.directory = "./orbitdb" + Math.random()

    satellite = await Satellite.createInstance()

    let initial = Math.random()
    db = await satellite.create("test", "counter-meta", options = {
      meta: { value: initial }
    })

    console.log(db.state)

    assert.equal(db.state.value, initial)
  })
})
