const Satellite = require("orbit-satellite")

const ipfsConfig = {}
const orbitdbConfig = {}

const putMusic = {
  name: "putMusic",
  apply(state, entry, { hash, identity }) {
    if(entry.author === identity.id) {
      state.music[hash] = entry
    }
    return state
  },
}

const delMusic = {
  name: "delMusic",
  apply(state, sheetToDel, { identity }) {
    if(state.music[sheetToDel].author == identity.id) {
      delete state.music[sheetToDel]
    }
     
    return state
  }
}

const putComment = {
  name: "putComment",
  apply(state, { musicHash, comment }, { hash, identity }) {
    state.comments[hash] = {
      music: musicHash,
      comment: comment,
      author: identity.id,
    }
    return state
  }
}

const delComment = {
  name: "delComment",
  apply(state, { hash }, { identity }) {
    if(state.comments[hash].author == identity.id) {
      delete state.comments[hash]
    }
    return state
  }
}

async function main() {
  Satellite.addStore({
    type: "music",
    operations: [putMusic, delMusic, putComment, delComment],
    initialState: {
      music: {},
      comments: {},
    },
  })

  const satellite = await Satellite.createInstance(ipfsConfig, orbitdbConfig)

  const db = await satellite.create("mymusic", "music")

  const { hash: musicHash } = await db.putMusic({ cid: "Qm...", author: satellite.identity.id })

  console.log("Add music: " + JSON.stringify(db.state)) // { cid: "Qm...", author: satellite.identity.toString() }

  
  const { hash: commentHash } = await db.putComment({
    musicHash: musicHash,
    comment: "I like it",
  })

  console.log("Add comment: " + JSON.stringify(db.state))

  await db.delComment({ hash: commentHash })

  console.log("Del comment: " + JSON.stringify(db.state))
  
  await db.delMusic(musicHash)

  console.log("Del Music: " + JSON.stringify(db.state)) // { comments: {}, music: {}}

  await satellite.stop()

  process.exit()
}

main()
